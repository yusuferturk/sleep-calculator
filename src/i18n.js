import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

const locale = 'en-US' // default locale

export default new VueI18n({
  locale,
  messages: {
    'en-US': {
      message: {
        title: 'Sleep Calculator',
        welcoming: 'Welcome to the sleep calculator!',
        option1: 'Do you want to go to bed now?',
        button1: 'Click!',
        option2: 'or will you wake up at a certain time?',
        button2: 'Click!',
        title2: 'Below you can set the time to wake up and see the times when you need to go to bed.',
        title3: 'If you are going to go to bed now, you have to wake in one those hours.',
        suggested: 'Suggested',
        return: 'Return',
      }
    },
    'tr-TR': {
      message: {
        title: 'Uyku Hesaplayıcısı',
        welcoming: 'Uyku hesaplayıcısına hoş geldin!',
        option1: 'Şimdi mi uyuyacaksınız?',
        button1: 'Tıklayın!',
        option2: 'ya da belirli bir saatte mi uyanacaksınız?',
        button2: 'Tıklayın!',
        title2: 'Aşağıda uyanacağınız zamanı belirleyip yatağa girmeniz gereken zamanları görebilirsiniz.',
        title3: 'Şimdi uyuyacaksınız aşağıdaki saatlerden birinde kalkmalısınız.',
        suggested: 'Önerilen',
        return: 'Geri Dön',
      }
    }
  }
})