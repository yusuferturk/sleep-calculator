import Vue from 'vue'
import VueRouter from 'vue-router'
import ThePages from '../components/ThePages.vue'
import CertainTime from '../components/CertainTime.vue'
import GoToBedNow from '../components/GoToBedNow.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: ThePages
  },
  {
    path: '/gotobednow',
    name: 'GoToBedNow',
    component: GoToBedNow
  },
  {
    path: '/certaintime',
    name: 'CertainTime',
    component: CertainTime
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
